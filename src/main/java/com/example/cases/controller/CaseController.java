package com.example.cases.controller;


import com.example.cases.model.CaseModel;
import com.example.cases.service.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cases")
public class CaseController {

    @Autowired
    private CaseService caseService;

    @GetMapping
    public List<CaseModel> getAllCases() {
        return caseService.getAllCases();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CaseModel> getCaseById(@PathVariable Long id) {
        Optional<CaseModel> caseModel = caseService.getCaseById(id);
        return caseModel.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public CaseModel createCase(@RequestBody CaseModel caseModel) {
        return caseService.createCase(caseModel);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CaseModel> updateCase(@PathVariable Long id, @RequestBody CaseModel caseDetails) {
        CaseModel updatedCase = caseService.updateCase(id, caseDetails);
        if (updatedCase != null) {
            return ResponseEntity.ok(updatedCase);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCase(@PathVariable Long id) {
        caseService.deleteCase(id);
        return ResponseEntity.noContent().build();
    }
}
