package com.example.cases.service;
import com.example.cases.model.CaseModel;
import com.example.cases.repository.CaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CaseService {

    @Autowired
    private CaseRepository caseRepository;

    public List<CaseModel> getAllCases() {
        return caseRepository.findAll();
    }

    public Optional<CaseModel> getCaseById(Long caseId) {
        return caseRepository.findById(caseId);
    }

    public CaseModel createCase(CaseModel caseModel) {
        caseModel.setCreationDate(LocalDateTime.now());
        caseModel.setLastUpdateDate(LocalDateTime.now());
        return caseRepository.save(caseModel);
    }

    public CaseModel updateCase(Long caseId, CaseModel caseDetails) {
        Optional<CaseModel> caseOptional = caseRepository.findById(caseId);
        if (caseOptional.isPresent()) {
            CaseModel caseToUpdate = caseOptional.get();
            caseToUpdate.setTitle(caseDetails.getTitle());
            caseToUpdate.setDescription(caseDetails.getDescription());
            caseToUpdate.setLastUpdateDate(LocalDateTime.now());
            return caseRepository.save(caseToUpdate);
        }
        return null;
    }

    public void deleteCase(Long caseId) {
        caseRepository.deleteById(caseId);
    }
}