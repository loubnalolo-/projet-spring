package com.example.cases.repository;

import com.example.cases.model.CaseModel;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CaseRepository extends JpaRepository<CaseModel, Long> {
}