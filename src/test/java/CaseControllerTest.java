


import com.example.cases.controller.CaseController;
import com.example.cases.model.CaseModel;
import com.example.cases.service.CaseService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CaseController.class)
public class CaseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CaseService caseService;

    @Test
    public void testGetCaseById() throws Exception {
        CaseModel testCase = new CaseModel();
        testCase.setCaseId(1L);
        testCase.setTitle("Test Case");
        testCase.setDescription("Test Description");
        testCase.setCreationDate(LocalDateTime.now());
        testCase.setLastUpdateDate(LocalDateTime.now());

        when(caseService.getCaseById(1L)).thenReturn(Optional.of(testCase));

        mockMvc.perform(get("/cases/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value("Test Case"))
                .andExpect(jsonPath("$.description").value("Test Description"));
    }

    @Test
    public void testCreateCase() throws Exception {
        CaseModel testCase = new CaseModel();
        testCase.setTitle("New Case");
        testCase.setDescription("New Description");

        when(caseService.createCase(any(CaseModel.class))).thenReturn(testCase);

        mockMvc.perform(post("/cases")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"title\": \"New Case\", \"description\": \"New Description\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value("New Case"))
                .andExpect(jsonPath("$.description").value("New Description"));
    }
}
